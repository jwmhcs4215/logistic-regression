# Logistic Regression

Logistic regression is a popular method to predict a categorical response. It is a special case of Generalized Linear models that predicts the probability of the outcomes. In spark.ml logistic regression can be used to predict a binary outcome by using binomial logistic regression, or it can be used to predict a multiclass outcome by using multinomial logistic regression
elasticNetParam corresponds to alpha and regParam corresponds to lambda.
MaxIter is the maximum number of iterations to train it for.

